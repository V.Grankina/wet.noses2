import Header from "./header/Header";
import * as React from 'react';
import { Outlet } from "react-router-dom";

function Layout({favoriteProducts, addedToCart}){
    return(
        <>
            <Header favorite={favoriteProducts} cart={addedToCart}/>
            <main>
                <Outlet/>
            </main>
            <footer>
                <div className="footer-copyright text-center py-3">© 2022 Copyright</div>
            </footer>
        </>
    )
}

export default Layout;