import React from "react";
import {Link} from "react-router-dom";

function Favorite({added}) {
    return (<Link to="/favorite" style={{textDecoration: "none", color: "white"}}>
            <img
                src={process.env.PUBLIC_URL + '/images/icons8-favorite-100.png'}
                width="50"
                height="50"
                className="d-inline-block align-top"
                alt=""
            />
            {added}
        </Link>
    )
}

export default Favorite;