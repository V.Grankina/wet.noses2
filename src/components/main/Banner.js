import Carousel from 'react-bootstrap/Carousel';
import React from "react";

function Banner() {
    return (
        <Carousel className="banner__image d-block">
            <Carousel.Item>
                <img
                    className="banner__image d-block"
                    src={process.env.PUBLIC_URL + '/images/banner-1.jpeg'}
                    alt="Cat drinks water"
                />
                <Carousel.Caption>
                    <h3>New Drinking Expiriense</h3>
                    <p>Increase cat water intake</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block banner__image"
                    src={process.env.PUBLIC_URL + '/images/banner-3.webp'}
                    alt="Cat drinks water"
                />
                <Carousel.Caption>
                    <h3>New Drinking Experience</h3>
                    <p>Increase cat water intake</p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
}

export default Banner;