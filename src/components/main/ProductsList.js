import React from "react";
import ProductCard from './Card';
import {useSelector} from "react-redux";

function ProductsList({onOpenModal}) {
    const products = useSelector(state => state.products.items);
    return (
        <div className="d-flex justify-content-between flex-wrap pt-5">
            {products.map(product => {
                return <ProductCard
                    key={product.id}
                    product={product}
                    onOpenModal={() => onOpenModal(product)}
                />
            })}
        </div>
    )
}

export default ProductsList;