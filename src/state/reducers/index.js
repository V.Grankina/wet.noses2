import {combineReducers} from "redux";

import {reducer as modalReducer} from "./modal";
import {reducer as productReducer} from "./products";
import {reducer as favoriteProductsReducer} from "./favoriteProducts";
import {reducer as cartReducer} from "./cart";

export default combineReducers({
    modal: modalReducer,
    products: productReducer,
    favoriteProducts: favoriteProductsReducer,
    cart: cartReducer,
})