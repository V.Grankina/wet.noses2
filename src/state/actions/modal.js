export const MODAL_WINDOW_OPEN = "MODAL_WINDOW_OPEN";
export const MODAL_WINDOW_CLOSE = "MODAL_WINDOW_CLOSE";

export const modalOpen = (text= "", confirmFn) => ({type: MODAL_WINDOW_OPEN, payload:{text, confirmFn}});
export const modalClose = () => ({type: MODAL_WINDOW_CLOSE});
