import Card from 'react-bootstrap/Card';
import React from "react";
import {useDispatch} from "react-redux";
import {minusProduct, plusProduct, removeFromCart} from "../../state/actions/cart";
import {modalClose, modalOpen} from "../../state/actions/modal";

function CardForCartPage({product}) {
    const dispatch = useDispatch();
    const onRemoveFromCart = (id) => {
        dispatch(removeFromCart(id))
        dispatch(modalClose())
    }
    const onRemove = (id) => {
        console.log(id)
        dispatch(modalOpen("Do you want to remove this beautiful fountain from your cart?",
            () => {
            onRemoveFromCart(id)}))
    };
    const onPlus = (id) => {dispatch(plusProduct(id))};
    const onMinus = (product) => {
        if (product.inCart ===1){
            dispatch(removeFromCart(product.id))
        } else {
            dispatch(minusProduct(product.id))
        }
    };
    return (<Card className="m-2">
            <button type="button" className="btn-close" aria-label="Close" onClick={()=>onRemove(product.id)}></button>
            <Card.Body className='d-flex align-items-end justify-content-between'>
                <div className='d-flex align-items-end'>
                <img className="mx-4"
                    src={process.env.PUBLIC_URL + `${product.image}`}
                     width="55px"
                     height="55px"
                     alt=""
                />
                    <button type="button" className="btn btn-light" onClick={()=>onMinus(product)}>-</button>
                    <h4 className="mx-4">{product.inCart}</h4>
                    <button type="button" className="btn btn-light" onClick={()=>onPlus(product.id)}>+</button>
                <h4 className="mx-4">{product.name}</h4>
                </div>
                <div className='d-flex align-items-end'>
                    <h4>Price: {product.price*product.inCart} USD</h4>
                </div>
            </Card.Body>
        </Card>
    )
}

export default CardForCartPage;