import React from 'react';
import Container from "react-bootstrap/Container";
import {Link} from "react-router-dom";
import { useSelector} from "react-redux";
import ProductCard from "../../components/main/Card";

function FavoritePage({onOpenModal}) {
    const products = useSelector(state => state.favoriteProducts.items)
    const productList = useSelector(state => state.products.items);
    if (products.length === 0) {
        return (<div className="d-flex flex-column align-items-center justify-content-center vh-100">
            <p className="fs-3"><span className="text-danger">Opps!</span> There is nothing here!</p>
            <Link to="/home" className="btn btn-info text-light">Go Home</Link>
        </div>)
    }
    const favoriteProducts = productList.filter( product => products.some(value=> value===product.id) )
    return (<Container>
            <div className="d-flex justify-content-between flex-wrap pt-5">
                {favoriteProducts.map(product => {
                    return <ProductCard
                        key={product.id}
                        product={product}
                        onOpenModal={() => onOpenModal(product)}
                    />
                })}
            </div>
        </Container>
    )
}

export default FavoritePage;